Cyber Security Operations

o	Strategies for implementing system policy and controls to defend against offensive operations in contested cyber environments. System vulnerability types/classes and methods of exploitation. Penetration testing. Malware types/classes, utilization, detection, isolation and analysis, and counter-forensics
